+++
# Contact widget.
widget = "contact"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 130  # Order that this section will appear.

title = "Contact"
subtitle = ""

# Automatically link email and phone?
autolink = true

# Email form provider
#   0: Disable email form
#   1: Netlify (requires that the site is hosted by Netlify)
#   2: formspree.io
email_form = 2
+++

### Sponsors

<img src="/img/dda_logo.png" alt="Danish Diabetes Academy", width="210">

<img src="/img/sdca-logo.jpg" alt="Steno Diabetes Center Aarhus", width="180">

<img src="/img/au_logo_black.png" alt="Aarhus University", width="250">
