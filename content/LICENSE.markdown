---
title: "Licensing and copyright information"
---



<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>

The course material is licensed under the
[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)
and the course code is licensed under a [MIT License](https://gitlab.com/lwjohnst/dda-rcourse) by Luke W. Johnston.

The teaching material also uses modified material from other courses and
workshops:

- [UofTCoders Reproducible Quantitative Methods course](https://uoftcoders.github.io/rcourse/)
([licensed](https://uoftcoders.github.io/rcourse/LICENSE.html) under CC-BY 4.0 International and MIT License).
- [Data Carpentry](http://datacarpentry.org) ([licensed](https://datacarpentry.org/lessons/) under the
[CC-BY 2.0 Generic License](https://creativecommons.org/licenses/by/2.0/))
- [Reproducible Quantitative Methods Course](https://cbahlai.github.io/rqm-template/)
(licensed under the [CC-BY 4.0 International License](https://creativecommons.org/licenses/by/4.0/))

The course material uses some concepts, images, and inspiration from these resources:

- [R for Data Science](http://r4ds.had.co.nz/)
- [Fundamentals of Data Visualization](https://serialmentor.com/dataviz/)

